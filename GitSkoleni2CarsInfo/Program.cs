﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GitSkoleni2Shared;

namespace GitSkoleni2CarsInfo
{
    class Program
    {
        // submodule with Car class can be found here: https://KirillMetrik@bitbucket.org/KirillMetrik/gitskoleni2shared.git

        static void Main(string[] args)
        {
            Car[] cars = new Car[]
            {
                new Car {Name="Ferrari", Color="Red", Cena="120tis." },
                new Car {Name="Lamborghini", Color="Yellow", Cena="120tis." },
                new Car {Name="Mercedes",  Color="Black", Cena="120tis."},
                new Car {Name="Skoda", Color="Green", Cena="200tis." }
            };
        }
    }
}
